﻿# Host: localhost  (Version 5.5.5-10.1.26-MariaDB)
# Date: 2018-04-05 00:39:02
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "buku"
#

DROP TABLE IF EXISTS `buku`;
CREATE TABLE `buku` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `pengarang` varchar(255) DEFAULT NULL,
  `sinopsis` varchar(255) DEFAULT NULL,
  `tgl_terbit` date DEFAULT NULL,
  `penerbit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "buku"
#

INSERT INTO `buku` VALUES (1,'Sassskeehhh','Masashi Kishimoto','Ninja cilik','1999-02-20','Namco'),(2,'One Piece','Odachi','asdasfas','2018-04-03','Namto'),(23,'Tokyo ghoul','fasdas','watashi wa psikotest desu','2018-04-09','fadsafads');

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `noinduk` varchar(10) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` enum('0','1','2') DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'1512501717','Hendry Nugroho','f4dfad803df83144a5be86d9bca87678','0'),(2,'1511501234','Pandhu','25070726dfd7c0943fa441946c27ecfe','1'),(3,'1511505678','Hilmi','54befaa93e2d128272e23714eb37b6a7','2');
