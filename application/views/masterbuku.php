<?php $this->load->view('header');?>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?=base_url('Admin/index')?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Master Buku</li>
		</ol>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<center>
					<a type="button" class="btn btn-primary" href="<?=base_url('Admin/newdata')?>">Add Data</a>
				</center>
			</div>
		</div>
		<hr>
		<!-- Icon Cards-->
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>Judul</th>
								<th>Pengarang</th>
								<th>Sinopsis</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<?php foreach($buku as $b) { ?>
						<tbody>
							<tr>
								<td>
									<?php echo $b->Id; ?>
								</td>
								<td>
									<?php echo $b->judul; ?>
								</td>
								<td>
									<?php echo $b->pengarang; ?>
								</td>
								<td>
									<?php echo $b->sinopsis; ?>
								</td>
								<td>
									<a type="button" class="btn btn-primary" href="<?=base_url('Admin/Edit/'.$b->Id.'') ?>">Edit</a>
									<?php if($this->session->userdata('level')=='0'){ ?>
									<a type="button" class="btn btn-danger" href="<?=base_url('Admin/DeleteData/'.$b->Id.'') ?>">Delete</a>
									<?php } ?>
									<a 
									href="javascript:;" 
									data-id="<?php echo $b->Id; ?>" 
									data-judul="<?php echo $b->judul; ?>" 
									data-tglterbit="<?php echo $b->tgl_terbit; ?>"
									data-penerbit="<?php echo $b->penerbit; ?>" 
									data-pengarang="<?php echo $b->pengarang; ?>" 
									data-sinopsis="<?php echo $b->sinopsis; ?>"
									data-toggle="modal" data-target="#det-data">
										<button data-toggle="modal" data-target="#detil-data" class="btn btn-success">Detil</button>
									</a>
								</td>
							</tr>
						</tbody>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container-fluid-->
	<!-- /.content-wrapper-->


	<!-- Modal Detil -->
	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="det-data" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 class="modal-title">Detil Data</h4>
				</div>
				<form role="form">
					<div class="modal-body">
						<div class="form-group">
							<label class="col-lg-2 col-sm-2 control-label">Judul</label>
							<div class="col-lg-10">
								<input type="hidden" id="id" name="id">
								<input type="text" readonly class="form-control" id="judul" name="judul" placeholder="Tuliskan judul">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-sm-2 control-label">Penerbit</label>
							<div class="col-lg-10">
								<input type="text" readonly class="form-control" id="penerbit" name="penerbit" placeholder="Tuliskan penerbit">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-sm-2 control-label">Pengarang</label>
							<div class="col-lg-10">
								<input type="text" readonly class="form-control" id="pengarang" name="pengarang" placeholder="Tuliskan pengarang">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-sm-2 control-label">Sinopsis</label>
							<div class="col-lg-10">
								<input type="text" readonly class="form-control" id="sinopsis" name="sinopsis" placeholder="Tuliskan sinopsis">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-sm-2 control-label">Terbit</label>
							<div class="col-lg-10">
								<input type="text" readonly class="form-control" id="tglterbit" name="tglterbit" placeholder="Tuliskan Tanggal Terbit">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- END Modal Detil -->
<script src="<?=base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script>
    $(document).ready(function() {
        // Untuk sunting
        $('#det-data').on('show.bs.modal', function (event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal          = $(this)
 
            // Isi nilai pada field
            modal.find('#id').attr("value",div.data('id'));
            modal.find('#judul').attr("value",div.data('judul'));
            modal.find('#penerbit').attr("value",div.data('penerbit'));
            modal.find('#pengarang').attr("value",div.data('pengarang'));
			modal.find('#tglterbit').attr("value",div.data('tglterbit'));
			modal.find('#sinopsis').attr("value",div.data('sinopsis'));
        });
    });
</script>
<?php $this->load->view('footer');?>
