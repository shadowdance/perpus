<?php $this->load->view('header');?>
<div class="content-wrapper">
	<div class="container-fluid">
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?=base_url('Admin/index')?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Form Buku</li>
		</ol>
		<!-- Icon Cards-->
		<?php if(isset($data)) { ?>
		<div class="row">
			<div class="col-md-12">
				<form action="">
					<input type="hidden" id="eid" name="id" value="<?php echo $this->uri->segment(3);?>" />
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="judul">Judul</label>
							<input type="text" class="form-control" id="ejudul" name="judul" placeholder="Judul" value="<?php echo $data->judul; ?>">
						</div>
						<div class="form-group col-md-6">
							<label for="tgl">Tanggal terbit</label>
							<input type="date" class="form-control" id="etglterbit" name="tglterbit" placeholder="Tanggal Terbit" value="<?php echo $data->tgl_terbit; ?>">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="Pengarang">Pengarang</label>
							<input type="text" class="form-control" id="epengarang" name="pengarang" placeholder="Pengarang" value="<?php echo $data->pengarang; ?>">
						</div>
						<div class="form-group col-md-6">
							<label for="tgl">Penerbit</label>
							<input type="text" class="form-control" id="epenerbit" name="penerbit" placeholder="Penerbit" value="<?php echo $data->penerbit; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="Pengarang">Sinopsis</label>
						<textarea name="sinopsis" id="esinopsis" class="form-control" placeholder="Sinopsis..."><?php echo $data->sinopsis; ?></textarea>
					</div>
					<input type="button" class="btn btn-primary" value="Edit" id="formedit">
				</form>
			</div>
		</div>
		<?php }else{ ?>
		<div class="row">
			<div class="col-md-12">
				<form action="">
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="judul">Judul</label>
							<input required="true" type="text" class="form-control" id="judul" name="judul" placeholder="Judul">
						</div>
						<div class="form-group col-md-6">
							<label for="tgl">Tanggal terbit</label>
							<input required="true" type="date" class="form-control" id="tglterbit" name="tglterbit" placeholder="Tanggal Terbit">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="Pengarang">Pengarang</label>
							<input required="true" type="text" class="form-control" id="pengarang" name="pengarang" placeholder="Pengarang">
						</div>
						<div class="form-group col-md-6">
							<label for="tgl">Penerbit</label>
							<input required="true" type="text" class="form-control" id="penerbit" name="penerbit" placeholder="Penerbit">
						</div>
					</div>
					<div class="form-group">
						<label for="Pengarang">Sinopsis</label>
						<textarea required="true" name="sinopsis" id="sinopsis" class="form-control" placeholder="Sinopsis..."></textarea>
					</div>
					<input type="button" class="btn btn-primary" id="forminput" value="Simpan">
				</form>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
<script src="<?=base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script type="text/javascript"> 
  $(document).ready(function(){
        $("#forminput").click(function(){
            var judul  = $('#judul').val();
            var tglterbit     = $('#tglterbit').val();
            var pengarang        = $('#pengarang').val();
			var penerbit        = $('#penerbit').val();
			var sinopsis        = $('#sinopsis').val();
            // var name     = $('#name').val();
            $.ajax({
                type:"POST",
                url :"<?php echo base_url(); ?>" + "Admin/InsertData",
                data:{
                    "judul":judul,
                    "tglterbit":tglterbit,
                    "pengarang":pengarang,
					"penerbit":penerbit,
					"sinopsis":sinopsis
                },
                success:function(html){
                    swal({
						title: "Success",
						text: "Input data berhasil!",
						icon: "success",
						timer: 2000
					}).then(function() {
						// Redirect the user
						window.location.href = "<?php echo base_url(); ?>"+"Admin/dataBuku";
						console.log('The Ok Button was clicked.');
					});
                }
            });
        });
    });
</script>

<script type="text/javascript"> 
  $(document).ready(function(){
        $("#formedit").click(function(){
			var id  = $('#eid').val();
            var judul  = $('#ejudul').val();
            var tglterbit     = $('#etglterbit').val();
            var pengarang        = $('#epengarang').val();
			var penerbit        = $('#epenerbit').val();
			var sinopsis        = $('#esinopsis').val();
            // var name     = $('#name').val();
            $.ajax({
                type:"POST",
                url :"<?php echo base_url(); ?>" + "Admin/EditData",
                data:{
					"id":id,
                    "judul":judul,
                    "tglterbit":tglterbit,
                    "pengarang":pengarang,
					"penerbit":penerbit,
					"sinopsis":sinopsis
                },
                success:function(html){
                    swal({
						title: "Success",
						text: "Edit data berhasil!",
						icon: "success",
						timer: 2000
					}).then(function() {
						// Redirect the user
						window.location.href = "<?php echo base_url(); ?>"+"Admin/dataBuku";
						console.log('The Ok Button was clicked.');
					});
                }
            });
        });
    });
</script>
<?php $this->load->view('footer');?>
