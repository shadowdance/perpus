<?php $this->load->view('header');?>
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<?php foreach($bukus as $bs) { ?>
			<div class="col-md-3">
				<div class="card text-center">
					<div class="card-header">
						Baru
					</div>
					<div class="card-body">
						<h5 class="card-title text-capitalize">
							<?php echo $bs->judul; ?>
						</h5>
						<p class="card-text">
							<?php echo $bs->sinopsis; ?>
						</p>
						<a href="javascript:;" data-id="<?php echo $bs->Id; ?>" data-judul="<?php echo $bs->judul; ?>" data-tglterbit="<?php echo $bs->tgl_terbit; ?>"
						data-penerbit="<?php echo $bs->penerbit; ?>" data-pengarang="<?php echo $bs->pengarang; ?>" data-sinopsis="<?php echo $bs->sinopsis; ?>"
						data-toggle="modal" data-target="#det-data">
							<button data-toggle="modal" data-target="#detil-data" class="btn btn-success">Detil</button>
						</a>
					</div>
					<div class="card-footer text-muted">
						Tahun Terbit :
						<?php echo $bs->tgl_terbit; ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>


<!-- Modal Detil -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="det-data" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h4 class="modal-title">Detil Data</h4>
			</div>
			<form role="form">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">Judul</label>
						<div class="col-lg-10">
							<input type="hidden" id="id" name="id">
							<input type="text" readonly class="form-control" id="judul" name="judul" placeholder="Tuliskan judul">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">Penerbit</label>
						<div class="col-lg-10">
							<input type="text" readonly class="form-control" id="penerbit" name="penerbit" placeholder="Tuliskan penerbit">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">Pengarang</label>
						<div class="col-lg-10">
							<input type="text" readonly class="form-control" id="pengarang" name="pengarang" placeholder="Tuliskan pengarang">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">Sinopsis</label>
						<div class="col-lg-10">
							<input type="text" readonly class="form-control" id="sinopsis" name="sinopsis" placeholder="Tuliskan sinopsis">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">Terbit</label>
						<div class="col-lg-10">
							<input type="text" readonly class="form-control" id="tglterbit" name="tglterbit" placeholder="Tuliskan Tanggal Terbit">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
<!-- END Modal Detil -->
<script src="<?=base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script>
	$(document).ready(function () {
		// Untuk sunting
		$('#det-data').on('show.bs.modal', function (event) {
			var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
			var modal = $(this)

			// Isi nilai pada field
			modal.find('#id').attr("value", div.data('id'));
			modal.find('#judul').attr("value", div.data('judul'));
			modal.find('#penerbit').attr("value", div.data('penerbit'));
			modal.find('#pengarang').attr("value", div.data('pengarang'));
			modal.find('#tglterbit').attr("value", div.data('tglterbit'));
			modal.find('#sinopsis').attr("value", div.data('sinopsis'));
		});
	});

</script>
<?php $this->load->view('footer');?>
