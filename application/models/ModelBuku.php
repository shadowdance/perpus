<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ModelBuku extends CI_Model {

    public function __construct(){
		parent::__construct();
	}

	public function InsertBuku($data){
		
		$checkinsert = false;
		
		try{
			
			$this->db->insert('buku',$data);
			$checkinsert = true;
		}catch (Exception $ex) {
			
			$checkinsert = false;
		}
		
		return $checkinsert; 
		
	
	}
	
	public function getAllBuku(){
		
		$result = $this->db->get('buku');
		return $result->result();
	}

	
	public function getBuku($id){
		
		$result = $this->db->where('Id',$id)->get('buku');
		return $result->row();
		
	}
	
	public function UpdateBuku($id,$data){
		$checkupdate = false;
		
		try{
			$this->db->where('Id',$id);
			$this->db->update('buku',$data);
			$checkupdate = true;
		}catch (Exception $ex) {
			
			$checkupdate = false;
		}
		
		return $checkupdate; 
		
	}
	
	public function DeleteBuku($id){
		$checkupdate = false;
		
		try{
			$this->db->where('Id',$id);
			$this->db->delete('buku');
			$checkupdate = true;
		}catch (Exception $ex) {
			
			$checkupdate = false;
		}
		
		return $checkupdate; 
		
	}
}
