<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();	
		$this->load->model('ModelUsers');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function auth(){
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$checkUsername = $this->ModelUsers->readUsername($username,$password);
	
	
		if($checkUsername==NULL){

			$this->load->view('login');
				
		}else if($checkUsername==1){
			$newdata = array(
				'username'  => $checkUsername->noinduk,
				'nama'  => $checkUsername->nama,
				'level' => $checkUsername->level
			  );
			//set seassion
			$this->session->set_userdata($newdata);
			if($this->session->userdata('level')=='0')
			{
				redirect('Admin/dataBuku');
			}
			else if($this->session->userdata('level')=='1')
			{
				redirect('Admin/dataBuku');
			}
			else if($this->session->userdata('level')=='2')
			{
				redirect('Peminjam/index');
			}
		}
	}
	
	public function logout(){
		
		$sess_array = $this->session->all_userdata();
		
		foreach($sess_array as $key =>$val){
		if($key!='username'
		  && $key!='nama'  
		  && $key!='RESERVER_KEY_HERE')$this->session->unset_userdata($key);
		}
		redirect('');
		//redirect to default controller and index function
		//$this->load->view('Login2');	
		
		
	}
	
	public function ShowSession(){
		
		$username = $this->session->username;
		
		$data['username'] = $username;
		
		$this->load->view('ShowSession',$data);
	}
	
}
