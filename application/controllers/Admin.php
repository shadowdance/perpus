<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
		$this->load->model('ModelUsers');
		$this->load->model('ModelBuku');
	}

	public function index()
	{
		if($this->session->username==NULL){
			redirect('');	
		}
		
		$nama = $this->session->name;
		$data['nama'] = $nama;
		$this->load->view('Admin',$data);
	}

	public function dataBuku()
	{
		$data['buku'] = $this->ModelBuku->getAllBuku();
		$this->load->view('masterbuku', $data);
	}

	public function listBuku()
	{
		$data['bukus'] = $this->ModelBuku->getAllBuku();
		$this->load->view('listbuku', $data);
	}

	//insert view
	public function Add(){
		if($this->session->name==NULL){
			redirect('');	
		}
		$nama = $this->session->name;
		$data['nama'] = $nama;
		$this->load->view('formbuku',$data);
	}
	
	//edit view
	public function Edit($id){
		$uname = $this->session->username;
		$data['uname'] = $uname;

		$result = $this->ModelBuku->getBuku($id);
		
		$data['data'] = $result;
		
		$this->load->view('formbuku',$data);
	}
	
	public function NewData(){
		$uname = $this->session->username;
		$data['uname'] = $uname;
		$this->load->view('formbuku', $data);
	}
	
	public function Delete(){
		$this->load->view('Modal/ConfirmationDeleteBuku');
	}	
	
	//insert
	public function InsertData(){
		
		$judul = $this->input->post('judul');
		$tglterbit = $this->input->post('tglterbit');
		$pengarang = $this->input->post('pengarang');
		$penerbit = $this->input->post('penerbit');
		$sinopsis = $this->input->post('sinopsis');
		
		$data = array(
		'judul' =>$judul,
		'tgl_terbit'=> $tglterbit,
		'pengarang'=> $pengarang,
		'penerbit'=> $penerbit,
		'sinopsis'=> $sinopsis,
		);
		
		$result = $this->ModelBuku->InsertBuku($data);

		redirect('Admin/dataBuku');
	
	}
	
	public function EditData(){
		
		$judul = $this->input->post('judul');
		$tglterbit = $this->input->post('tglterbit');
		$pengarang = $this->input->post('pengarang');
		$penerbit = $this->input->post('penerbit');
		$sinopsis = $this->input->post('sinopsis');
		$id = $this->input->post('id');
		
		$data = array(
		'judul' =>$judul,
		'tgl_terbit'=> $tglterbit,
		'pengarang'=> $pengarang,
		'penerbit'=> $penerbit,
		'sinopsis'=> $sinopsis,
		);
		
		$result = $this->ModelBuku->UpdateBuku($id,$data);
		
		$data = NULL;
		if ($result){
			redirect('Admin/dataBuku');
		}else{
			redirect('Admin/dataBuku');
		}
	}
	
	public function DeleteData($id){
		$result = $this->ModelBuku->DeleteBuku($id);
		
		if ($result){
			redirect('Admin/dataBuku');
		}else{
			redirect('Admin/dataBuku');
		}
	}
	
}
